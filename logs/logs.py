import datetime

class Logs:
    
    def __init__(self):
        self.filename = 'data/logs/logs_{}.txt'.format(datetime.datetime.now().strftime("%Y-%m-%d"))
    
    def create_log_file(self):
        pass
    
    def write_logs(self, message):
        date = datetime.datetime.now().strftime("%d-%m-%Y_%H:%M:%S")
        with open(self.filename, 'a') as f:
            f.write('{} \t {} \n'.format(date, message))