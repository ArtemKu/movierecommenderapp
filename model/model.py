from component.sqlite import SQLite
from logs.logs import Logs
from configparser import ConfigParser
from recsys.recsys_svd import RecsysSVD
import pandas as pd


class Model:
    
    
    def __init__(self):
        self.config = ConfigParser()
        self.config.read('config/config.ini')

        self.l = Logs()
        self.r = RecsysSVD()
        self.s = SQLite(self.config['sqlite']['location'])
        self.all_movie_id = self.s.get_all_movie_id()
    
    def add_new_film(self, name, year, genres_list):
        title = '{} ({})'.format(name, year)
        genres = "|".join(genres_list.split(',')) 
        self.s.new_film(title, genres)
        self.all_movie_id = self.s.get_all_movie_id()
        
        
    def get_score_user_movie(self, user, movie):
        return self.r.predict_one(user,movie)
    
    
    def update_user_score(self, user_id, movie_id, rating):
        self.s.update_user_film_rating(user_id, movie_id, rating)
        
    
    def add_user_score(self, user_id, movie_id, rating):
        self.s.new_user_film_rating(user_id, movie_id, rating)
        
    
    def get_top_10_movies(self, user_id):
        
        user_movie_id = self.s.get_user_movies_view(user_id)
        user_movie_id = [i[0] for i in user_movie_id]
        unview_movie_id = list(set(self.all_movie_id).difference(set(user_movie_id)))
        unview_movie_id = [i[0] for i in unview_movie_id]
        user_id_list = [user_id for i in range(len(unview_movie_id))]
        score_films =  self.r.predict(user_id_list, unview_movie_id)
        df = pd.DataFrame(list(zip(user_id_list, unview_movie_id, score_films)),
               columns = ['user_id', 'movie_id', 'score'])
        df = df.sort_values(by=['score'], ascending=False)
        df = df.head(10) # выбрать топ 10
        movie_ids = df['movie_id'].values.tolist()
        movie_names = [self.s.get_movie_names(i) for i in movie_ids]
        return movie_names
        
        
    def get_top_k_movies(self, user_id, k):
        
        user_movie_id = self.s.get_user_movies_view(user_id)
        user_movie_id = [i[0] for i in user_movie_id]
        unview_movie_id = list(set(self.all_movie_id).difference(set(user_movie_id)))
        unview_movie_id = [i[0] for i in unview_movie_id]
        user_id_list = [user_id for i in range(len(unview_movie_id))]
        score_films =  self.r.predict(user_id_list, unview_movie_id)
        df = pd.DataFrame(list(zip(user_id_list, unview_movie_id, score_films)),
               columns = ['user_id', 'movie_id', 'score'])
        df = df.sort_values(by=['score'], ascending=False)
        if k < 1:
            k = 1
        elif k > len(unview_movie_id):
            k = len(unview_movie_id)
        df = df.head(k) # выбрать k
        movie_ids = df['movie_id'].values.tolist()
        movie_names = [self.s.get_movie_names(i) for i in movie_ids]
        
        return movie_names
        