from configparser import ConfigParser
from component.sqlite import SQLite
import pandas as pd
from logs.logs import Logs
from surprise import SVD, Reader, Dataset

l = Logs()
"""
config = ConfigParser()
config.read('config/config.ini')
"""


class RecsysSVD:
    
    
    def __init__(self):
        self.config = ConfigParser()
        self.config.read('config/config.ini')
        self.s = SQLite(self.config['sqlite']['location'])
       
        self.mdl_svd = SVD() 
       
        self.train_data_created()
        self.fit()
    
    def train_data_created(self):
        result = self.s.get_all_ratings_data()
        self.df = pd.DataFrame(result, columns = ['userID', 'movieID', 'score'])    
        un_score = self.df['rating'].unique()
        reader = Reader(rating_scale=(1,5))
        self.data = Dataset.load_from_df(self.df[['userID', 'itemID', 'rating']], reader)
        
    
    def fit(self):
     #   self.df_pivot = pd.pivot_table(self.df, values = 'score', index = 'user_id', columns = 'movie_id')
     #   self.df_pivot = self.df_pivot.fillna(0)
         self.mdl_svd.fit(self.data)

    
    #Какой оценку поставил первый пользователь какому-то фильму
    def predict_one(self, user_id, movie_id) :
        try:
            return self.mdl_svd(uid=user_id, iid=movie_id)
        except:
            return float(self.config['recsys']['mean_score'])
    
    #Какой оценку поставил первый пользователь ,какому-то фильму, какую оценку поставил второй и так далее
    def predict(self, user_id_list, movie_id_list):
        result = []
        for user, movie in zip(user_id_list, movie_id_list):
            result.append(self.predict_one(user, movie))
            
            
        