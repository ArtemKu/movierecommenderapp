from flask import Flask
from flask import request
from model.model import Model


app = Flask(__name__)
m = Model()


@app.route('/newfilm', methods = ['GET'])
def add_new_film():
    
    name = request.args.get('name')
    year = request.args.get('year')
    genres_list = request.args.get('genres_list')
    m.add_new_film(name, year, genres_list)
    
    return 'OK'



@app.route('/score/get', methods = ['GET'])
def get_score_user_movie():
    
    user = int(request.args.get('user'))
    movie = int(request.args.get('movie'))
    result = m.get_score_user_movie(user, movie)
    
    return str(result)
    

@app.route('/score/update', methods = ['GET'])
def update_score_user_movie():
    
    user_id = int(request.args.get('user'))
    movie_id = int(request.args.get('movie'))
    rating = float(request.args.get('rating'))
    m.update_score_user_movie(user_id, movie_id, rating)
    
    return 'OK'
        

@app.route('/score/add', methods = ['GET'])
def add_score_user_movie():
    
    user_id = int(request.args.get('user'))
    movie_id = int(request.args.get('movie'))
    rating = float(request.args.get('rating'))
    m.add_score_user_movie(user_id, movie_id, rating)
    
    return 'OK'

@app.route('/score/get_top10', methods = ['GET'])
def get_top_10_movies():
    
    user_id = int(request.args.get('user'))
    top_10_movie_names = m.get_top_10_movies(user_id)
    return '\n '.join(top_10_movie_names)

@app.route('/score/get_top_k', methods = ['GET'])
def get_top_k_movies():
    
    user_id = int(request.args.get('user'))
    k = int(request.args.get('k'))
    top_k_movie_names = m.get_top_k_movies(user_id, k)
    return '\n '.join(top_k_movie_names)



if __name__ == '__main__':
    app.run(port=5002)